## Interface: 11302
## Title: Player Database Classic - Guild
## Author: CiscOH-Blaumeux
## Notes: Database AddOn to collect data for use in other AddOns
## Version: 0.0.0
## DefaultState: enabled
## SavedVariables: PDBClassicGuildDB
## OptionalDeps: Ace3
## X-Embeds: Ace3

embeds.xml
enUS.lua
Core.lua